# delightful contributors

These fine people brought us delight by adding their gems of freedom to the delight project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](delight-us.md#attribution-of-contributors).

## We thank you for your gems of freedom :gem:

- ShellOwl (codeberg: [@shellowl](https://codeberg.org/shellowl), fediverse: [@shellowl@social.nixnet.services](https://social.nixnet.services/@shellowl))
- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))
